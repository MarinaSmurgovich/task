import java.io.*;
import java.nio.Buffer;
import java.util.*;
import java.util.logging.FileHandler;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;

public class FileManeger {
    private static int compare(File file1, File file2) {
        return file1.getName().compareTo(file2.getName());
    }

    private File rootDirectory;

    public FileManeger(String rootDirPath) {
        this.rootDirectory = new File(rootDirPath);
    }

    public List<File> getFileList() {
        File[] fileArray = rootDirectory.listFiles();
        if (fileArray != null) {
            Arrays.sort(fileArray, Comparator.comparing(File::getName));
        }
        return fileArray != null ? asList(fileArray) : emptyList();
    }

    public String reedFile(File file) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();

        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                stringBuilder.append(line)
                        .append("\n");
            }
        }
        return stringBuilder.toString();
    }

    void writeFile(String fileName, String fileText) throws IOException {
        File file = new File(rootDirectory, fileName);
        if (!file.exists()) {
            file.createNewFile();
        }
        try (FileWriter fileWriter = new FileWriter(file);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            bufferedWriter.write(fileText);
        }
    }

    void deleteFile(File file) {
        if (file.delete()) {
            System.out.println(file.getName() + " был удален");
        } else
            System.out.println("Файл не обнаружен");
    }

    void addNewText(File file, String str) throws IOException {
        FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(str);
        bw.close();
    }

}

