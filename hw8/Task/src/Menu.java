import exceptions.MenuActionException;
import menu.MainMenuConsts;
import menu.item.MainMenuAction;
import menu.item.MenuItem;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import static menu.MainMenuConsts.ACTION_ITEM_ERROR_MESSAGE;
import static menu.MainMenuConsts.WRITE_FILE_ERROR_MESSAGE;
import static menu.MenuFactory.getMainMenu;

public class Menu {

    private FileManeger fileManeger;
    private Scanner scanner;
    int m;
    String fileName;

    public Menu(String rootDirectory) {
        this.fileManeger = new FileManeger(rootDirectory);
        this.scanner = new Scanner(System.in);
    }

    void start() {
        while (true) {
            printMenuItems(getMainMenu(MainMenuAction.values()));
            try {
                int menuNumber = readIntFromConsole();
                MainMenuAction mainMenuAction = MainMenuAction.getMainMenuAction(menuNumber);
                mainMenuAction(mainMenuAction);
            } catch (MenuActionException | IOException e) {
                e.printStackTrace();
                printText(ACTION_ITEM_ERROR_MESSAGE);
            }
        }
    }

    private void mainMenuAction(MainMenuAction mainMenuAction) throws IOException {
        switch (mainMenuAction) {
            case REED_FILE:
                workWithFile();
                String str = String.valueOf(Calendar.getInstance().getTime()) + " reed file \"" + fileManeger.getFileList().get(m).getName() + "\" \n";
                Log.newTextAddForLogger(str);
                break;
            case ADD_FILE:
                addNewFileAction();
                String str1 = String.valueOf(Calendar.getInstance().getTime()) + " add file \"" + fileName + "\" \n";
                Log.newTextAddForLogger(str1);
                break;
            case DELETE_FILE:
                fileDelete();
                String str2 = String.valueOf(Calendar.getInstance().getTime()) + " delete file \"" + file1.getName() + "\" \n";
                Log.newTextAddForLogger(str2);
                break;
            case ADD_NEW_TEXT:
                newTextAdd();
                String str3 = String.valueOf(Calendar.getInstance().getTime()) + " add new text \"" + fileManeger.getFileList().get(m).getName() + "\" \n";
                Log.newTextAddForLogger(str3);
                break;
            case ADD_TO_ZIP:
                addToZip();
                String str4 = String.valueOf(Calendar.getInstance().getTime()) + " all files added to zip" + "\n";
                Log.newTextAddForLogger(str4);
                break;
            case MOVING_TO_BACKUP:
                movingZip(Consts.FILE_TO_BACKUP, Consts.BACKUP_PATH);
                String str5 = String.valueOf(Calendar.getInstance().getTime()) + " \"rootdir.zip\" moved to BackUP " + "\n";
                Log.newTextAddForLogger(str5);
                break;
            case BACKUP_REPLACEMENT:
                backupReplacement();
                String str6 = String.valueOf(Calendar.getInstance().getTime()) + " BACKUP_REPLACEMENT done" + "\n";
                Log.newTextAddForLogger(str6);
                break;
            case EXIT:
                System.exit(0);
        }
        ;
    }

    private void addToZip() throws IOException {
        List<File> files = fileManeger.getFileList();
        FileOutputStream fos = new FileOutputStream("rootdir.zip");
        ZipOutputStream zipOut = new ZipOutputStream(fos);
        for (File file : files) {
            File fileToZip = new File(String.valueOf(file));
            FileInputStream fis = new FileInputStream(fileToZip);
            ZipEntry zipEntry = new ZipEntry(fileToZip.getName());
            zipOut.putNextEntry(zipEntry);
            byte[] bytes = new byte[1024];
            int length;
            while ((length = fis.read(bytes)) >= 0) {
                zipOut.write(bytes, 0, length);
            }
            fis.close();
        }
        zipOut.close();
        fos.close();
    }

    private void movingZip(String filePath, String ditPath) {
        deleteAllFilesFolder(ditPath);
        File file = new File(filePath);
        File dir = new File(ditPath);
        file.renameTo(new File(dir, file.getName()));
    }

    private static void deleteAllFilesFolder(String path) {
        for (File myFile : new File(path).listFiles())
            if (myFile.isFile()) myFile.delete();
    }

    private void backupReplacement() throws IOException {
        List<File> files = fileManeger.getFileList();
        for (File file : files) {
            file.delete();
        }
        unZip();
    }

    private void unZip() throws IOException {
        String fileZip = Consts.FILE_BACK_PATH;
        File destDir = new File(Consts.ROOT_DIR_PATH);
        byte[] buffer = new byte[1024];
        ZipInputStream zis = new ZipInputStream(new FileInputStream(fileZip));
        ZipEntry zipEntry = zis.getNextEntry();
        while (zipEntry != null) {
            File newFile = newFile(destDir, zipEntry);
            FileOutputStream fos = new FileOutputStream(newFile);
            int len;
            while ((len = zis.read(buffer)) > 0) {
                fos.write(buffer, 0, len);
            }
            fos.close();
            zipEntry = zis.getNextEntry();
        }
        zis.closeEntry();
        zis.close();
    }

    public static File newFile(File destinationDir, ZipEntry zipEntry) throws IOException {
        File destFile = new File(destinationDir, zipEntry.getName());

        String destDirPath = destinationDir.getCanonicalPath();
        String destFilePath = destFile.getCanonicalPath();

        if (!destFilePath.startsWith(destDirPath + File.separator)) {
            throw new IOException("Entry is outside of the target dir: " + zipEntry.getName());
        }

        return destFile;
    }

    private void addNewFileAction() {

        fileName = readStringFromConsole();
        String fileTest = readStringLines();

        try {
            fileManeger.writeFile(fileName, fileTest);
        } catch (IOException e) {
            e.printStackTrace();
            printText(WRITE_FILE_ERROR_MESSAGE);
        }
    }

    private void newTextAdd() {
        List<File> files = fileManeger.getFileList();
        printFiles(files);
        m = readIntFromConsole();
        File file = files.get(m);

        String fileText = readStringLines();
        try {
            fileManeger.addNewText(file, fileText);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void workWithFile() {
        List<File> files = fileManeger.getFileList();
        printFiles(files);
        m = readIntFromConsole();
        File file = files.get(m);

        String text = "";
        try {
            text = fileManeger.reedFile(file);
        } catch (IOException e) {
            e.printStackTrace();
            text = MainMenuConsts.READ_FILE_ERROR_MESSAGE;
        }
        printText(text);
    }

    File file1;

    private void fileDelete() {
        List<File> files = fileManeger.getFileList();
        printFiles(files);
        m = readIntFromConsole();
        File file = files.get(m);
        file1 = file;
        fileManeger.deleteFile(file);
    }


    private <T> void printText(T text) {
        System.out.println(text.toString());
    }

    private void printFiles(List<File> files) {
        for (int i = 0; i < files.size(); i++) {
            printText(String.format(MainMenuConsts.ITEM_WiTH_NUMBER, i, files.get(i).getName()));
        }
    }


    private void printMenuItems(List<MenuItem> menuItems) {
        for (MenuItem menuItem : menuItems) {
            printText(menuItem);
        }
    }

    private int readIntFromConsole() {
        int number = scanner.nextInt();
        scanner.nextLine();
        return number;
    }

    private String readStringFromConsole() {
        return scanner.nextLine();
    }

    private String readStringLines() {
        StringBuilder stringBuilder = new StringBuilder();
        String line = "";
        while ((line = readStringFromConsole()) != null && !line.equals("*exit")) {
            stringBuilder.append(line).append("\n");


        }
        return stringBuilder.toString();
    }
}
