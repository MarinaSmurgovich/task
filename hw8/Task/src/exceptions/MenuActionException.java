package exceptions;

public class MenuActionException extends Exception{
    public MenuActionException(String message) {
        super(message);
    }
}
