package Scaners;

public class Scaner {
    private int number;

    public Scaner(int number) {
        this.number = number;
    }

    public static int getNumber() {
        java.util.Scanner in = new java.util.Scanner(System.in);
        int number = in.nextInt();
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
