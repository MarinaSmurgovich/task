package ClassesSorters;

public class SelectionSort extends Sortable {

    public static void selectionSort1(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int min = array[i];
            int iMin = i;

            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < min) {
                    min = array[j];
                    iMin = j;
                }
            }
            if (i != iMin) {
                int temp = array[i];
                array[i] = array[iMin];
                array[iMin] = temp;
            }
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

    public static void selectionSort2(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int max = array[i];
            int iMax = i;

            for (int j = i + 1; j < array.length; j++) {
                if (array[j] > max) {
                    max = array[j];
                    iMax = j;
                }
            }
            if (i != iMax) {
                int temp = array[i];
                array[i] = array[iMax];
                array[iMax] = temp;
            }
            System.out.print(array[i] + " ");
        }
    }
}
