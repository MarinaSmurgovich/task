package ClassesSorters;

import java.util.Scanner;

public class Sortable {
    private int[] array;

    public static int[] getArray() {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите массив из 10 чисел: ");
        int array[] = new int [10];
        for (int i = 0; i < array.length; i++) {
            array[i] = in.nextInt();
        }
        return array;
    }

    public void setArray(int[] array) {
        this.array = array;
    }
}
