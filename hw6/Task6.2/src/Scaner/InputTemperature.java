package Scaner;

import java.util.Scanner;

public class InputTemperature {
    private double temperature;

    public static double getTemperature() {
        System.out.println("Введите температуру в градусах Цельсия: ");
        Scanner input = new Scanner(System.in);
        double temperature = input.nextDouble();
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }
}
