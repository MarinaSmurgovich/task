package conversion_temperature;

public class Kelvin extends Temperature {

    public Kelvin() {
    }

    @Override
    public void conversion(double t) {
        //0 °C + 273,15 = 273,15 K
        double result = t + 273.15;
        System.out.println("Результат конвертации: " + result);
    }
}
