import Scaner.InputTemperature;
import Scaner.Menu;
import conversion_temperature.Fahrenheit;
import conversion_temperature.Kelvin;

public class Main {
    public static void main(String[] args) {
        double t = InputTemperature.getTemperature();
        int number = Menu.getNumber();
        switch (number) {
            case 1:
                Fahrenheit fahrenheit = new Fahrenheit();
                fahrenheit.conversion(t);
                break;
            case 2:
                Kelvin kelvin = new Kelvin();
                kelvin.conversion(t);
                break;
            default:
                System.out.println("Введено неверное число");
        }
    }
}
