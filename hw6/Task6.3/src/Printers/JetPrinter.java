package Printers;

public class JetPrinter implements Printer {

    @Override
    public void introduceYourSelf() {
        System.out.println("Струйный принтер");

    }

    @Override
    public void print(String message) {
        System.out.println(message);
    }
}
