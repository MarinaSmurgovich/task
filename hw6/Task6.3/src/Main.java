import Printers.*;
import Scanners.ScanerNumber;
import Scanners.ScanerString;

public class Main {
    public static void main(String[] args) {
        JetPrinter jetPrinter = new JetPrinter();
        LaserPrinter laserPrinter = new LaserPrinter();
        Printer3D printer3D = new Printer3D();
        InterntPriner interntPriner = new InterntPriner();
        AdvertisingDisplay advertisingDisplay = new AdvertisingDisplay();

        int number;
        int num;

        do {
            System.out.print(" 1 - ");
            jetPrinter.introduceYourSelf();
            System.out.print(" 2 - ");
            laserPrinter.introduceYourSelf();
            System.out.print(" 3 - ");
            printer3D.introduceYourSelf();
            System.out.print(" 4 - ");
            interntPriner.introduceYourSelf();
            System.out.print(" 5 - ");
            advertisingDisplay.introduceYourSelf();

            number = ScanerNumber.scaner();
            String message = ScanerString.scaner();
            switch (number) {
                case 1:
                    jetPrinter.print(message);
                    num = Menu.menu();
                    if (num == 1) {
                        message = ScanerString.scaner();
                        jetPrinter.print(message);
                    }
                    break;
                case 2:
                    laserPrinter.print(message);
                    num = Menu.menu();
                    if (num == 1) {
                        message = ScanerString.scaner();
                        laserPrinter.print(message);
                    }
                    break;
                case 3:
                    printer3D.print(message);
                    num = Menu.menu();
                    if (num == 1) {
                        message = ScanerString.scaner();
                        printer3D.print(message);
                    }
                    break;
                case 4:
                    interntPriner.print(message);
                    num = Menu.menu();
                    if (num == 1) {
                        message = ScanerString.scaner();
                        interntPriner.print(message);
                    }
                    break;
                case 5:
                    advertisingDisplay.print(message);
                    num = Menu.menu();
                    if (num == 1) {
                        message = ScanerString.scaner();
                        advertisingDisplay.print(message);
                    }
                    break;
                default:
                    System.out.println("Принтер с таким порядковым номером не существует!");

            }
        }
        while (number == 1 || number == 2 || number == 3 || number == 4 || number == 5);
    }
}
