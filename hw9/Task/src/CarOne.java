public class CarOne extends Car {

    private final int gruz = 2000;

    public CarOne() {
    }

    @Override
    public int getGruz() {
        return gruz;
    }
}
