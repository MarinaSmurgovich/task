public class CreateRun extends Thread {

    private static Car car;
    private static String s1 = "s1";
    private static String s2 = "s2";
    private static String way = "way";

    public CreateRun(Car car) {
        this.car = car;
    }

    public static void loading() throws InterruptedException {
        synchronized (s1) {
            System.out.println(car.getGruz());
            Thread.sleep(car.getGruz());
            System.out.println(Thread.currentThread().getName() + " загрузился и выежает на склад разгрузки");
        }
    }

    public static void onWay() throws InterruptedException {
        synchronized (way) {
            System.out.println("5000");
//            System.out.println(Thread.currentThread().getName() + " выехал на склад разрузки");
            Thread.sleep(5000);
            System.out.println(Thread.currentThread().getName() + " прибыл на склад разгрузки");
        }
    }

    public static void unloading() throws InterruptedException {
        synchronized (s2) {
            System.out.println(car.getGruz()/2);
//            System.out.println(Thread.currentThread().getName() + " начал разгрузку");
            Thread.sleep(car.getGruz()/2);
            System.out.println(Thread.currentThread().getName() + " разгрузился и покинул склад разгрузки");
        }
    }

    @Override
    public void run() {
        try {
            loading();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            onWay();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            unloading();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

