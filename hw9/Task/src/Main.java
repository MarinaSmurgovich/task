import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Car> cars = new ArrayList<>();
        cars.add(new CarOne());
        cars.add(new CarOne());
        cars.add(new CarThree());
        cars.add(new CarThree());
        cars.add(new CarThree());
        cars.add(new CarThree());
        cars.add(new CarThree());
        cars.add(new CarFive());
        cars.add(new CarFive());
        cars.add(new CarFive());
//        Car car1 = new CarOne();
//        Car car2 = new CarOne();
//        Car car3 = new CarOne();
//
//        Car car4 = new CarThree();
//        Car car5 = new CarThree();
//        Car car6 = new CarThree();
//        Car car7 = new CarThree();
//
//        Car car8 = new CarFive();
//        Car car9 = new CarFive();
//        Car car10 = new CarFive();

        for (Car car : cars) {
            new CreateRun(car).start();
        }
//        new CreateRun(car1).start();
//        new CreateRun(car2).start();
//        new CreateRun(car3).start();
//        new CreateRun(car4).start();
//        new CreateRun(car5).start();
//        new CreateRun(car6).start();
//        new CreateRun(car7).start();
//        new CreateRun(car8).start();
//        new CreateRun(car9).start();
//        new CreateRun(car10).start();

}}

