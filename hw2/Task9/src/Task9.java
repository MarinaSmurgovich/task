/*9)	Получить первые N чисел из ряда
 Фибоначчи (использовать только циклы);*/

public class Task9 {
    public static void main(String[] args) {
        int n = 20;
        int f1 = 0;
        int f2 = 1;
        int f;
        System.out.print(f1 + " ");

        /** NOTE! В условии к циклу можно написать не i <= (n - 1), а
         * i < n. Суть та же, а кода меньше, и логика чище
         */
        for (int i = 1; i < n; i++) {
            if (i == 2) {
                f1 = f2;
            }
            if (i > 2) {
                f = f2 + f1;
                f1 = f2;
                f2 = f;
            }
            System.out.print(f2 + " ");
        }
    }
}




