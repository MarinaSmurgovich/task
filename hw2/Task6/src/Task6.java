/* 6)	Есть массив из десяти целых чисел.
 Необходимо заменить каждый четный элемент массива на его
 удвоенное произведение (т.е. возвести квадрат).
 Например, есть массив [1, 2, 3,4],
 тогда результатом должен быть [1, 4, 3, 16];*/

public class Task6 {
    public static void main(String[] args) {
        int[] array = new int[10];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 100 + 1);
            System.out.print(array[i] + " ");
        }
        System.out.println();
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0) {
                array[i] *= array[i];
            }
        }
        for (int i = 0; i < array.length; i++)
            System.out.print(array[i] + " ");
    }
}



