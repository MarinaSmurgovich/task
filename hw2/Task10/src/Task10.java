/*10)	Есть двумерный массив (MxN) целых чисел (от 0 до 9).
С помощью команд System.out.println и System.out.print вывести
в консоль весь массив как в примере ниже.
По возможности использовать чикл for-each;
1 2 4 5
8 9 7 0
6 6 4 3
*/
public class Task10 {
    public static void main(String[] args) {
        int[][] array = {{1, 2, 4, 5}, {8, 9, 7, 0}, {6, 6, 4, 3}};
        for (int[] arrayPart : array) {
            for (int i : arrayPart) {
                System.out.print(i + " ");
            }
            System.out.println();
        }
    }
}
