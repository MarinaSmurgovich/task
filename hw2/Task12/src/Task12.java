/*12)	Есть массив чисел с плавающей запятой.
 Необходимо реализовать сортировку методом выбора по возрастанию и по убыванию;*/

public class Task12 {
    public static void main(String[] args) {
        double[] array = {-10.5, 8.0, 19.0, 5.45, 6.38, -0.32, -1.5};

        System.out.print("Сортировка по возрастанию: ");
        for (int i = 0; i < array.length; i++) { // берем первый э-т и предполагаем, что он минимальный, запонимаем
            // его номер в массиве
            double min = array[i];
            int iMin = i;

            for (int j = i + 1; j < array.length; j++) { // ищем э-т, кот меньше первого эл-та, да - запонимаем номер
                if (array[j] < min) {
                    min = array[j];
                    iMin = j;
                }
            }
                if (i != iMin) { // меняем местами
                    double temp = array[i];
                    array[i] = array[iMin];
                    array[iMin] = temp;
                }
            System.out.print(array[i] + " ");
        }
        System.out.println();

        System.out.print("Сортировка по убыванию: ");
        for (int i = 0; i < array.length; i++) {
            double max = array[i];
            int iMax = i;

            for (int j = i + 1; j < array.length; j++) {
                if (array[j] > max) {
                    max = array[j];
                    iMax = j;
                }
            }
            if (i != iMax) {
                double temp = array[i];
                array[i] = array[iMax];
                array[iMax] = temp;
            }
            System.out.print(array[i] + " ");
        }
    }
}
