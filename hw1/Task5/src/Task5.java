/**
 * Задание № 5
 *
 * 1) Написать алгоритм округления числа до целого.
 * Например 2.5 -> 3, 2.6 -> 3, 2.4 -> 2
 *
 * Вызвать функцию в методе main, а результат вывести в консоль
 * Пример вызова:
 * int result = roundNumber(4.45)
 * System.out.println(result)
 *
 * 2) Написать алгоритм получения дробной части числа.
 * То есть, если у меня число 2.75, то я хочу получить 0.75
 *
 */

public class Task5 {

    public static int roundNumber(double number) {
        /**
         * NOTE! Лишние внешние круглые скобки.
         */
        return  (int) (number + 0.5);
    }

    public static double getTail(double number) {
        /**
         * NOTE! по сути приводить результат к double нет необходимости
         * потому, что в операции и так есть тип double
         */
        return number - (int)number;
    }

    public static void main(String[] args) {
        int result = roundNumber(4.5);
        System.out.println(result);

        double result1 = getTail(3.98);
        System.out.println(result1);
    }
}
