/**
 * Задание № 7
 * <p>
 * ++++ 1) Даны 3 числа. Вычислить их сумму. Если все три числа равны,
 * то нужно вернуть сумму увеличеннную в два раза
 * <p>
 * 2) Даны 2 числа, нужно вернуть true,
 * если одно из низ равно 10 или их сумма равно 10
 * <p>
 * 3) Даны 3 числа, нужно вернуть максимально число
 * Результат вывести в консоль
 * <p>
 * 4) Даны 3 числа, нужно вернуть минимальное число
 * Результат вывести в консоль
 */

public class Task8 {

    public static int sum1(int a, int b, int c) {
        /**
         * NOTE! Тут можно написать проще. Примерно так
         *
         * int res = a + b + c;
         * if (a == b && b==c && a==c) {
         *     res *= 2
         * }
         * return res
         */
        int res = a + b + c;
        if (a == b && b ==c && c == a) {
            res *= 2;
        }
        return res;
    }

    public static void sum2(int a, int b) {
        if (a == 10 || b == 10 || (a + b) == 10) {
            System.out.println("true");
        } else
            System.out.println("false");
    }

    public static void max(int a, int b, int c) {
        System.out.print("Максимальное число: ");
        if (a > b && a > c)
            System.out.println(a);
        else if (b > a && b > c)
            System.out.println(b);
        else
            System.out.println(c);
    }

    public static void min(int a, int b, int c) {
        System.out.print("Минимальное число: ");
        if (a < b && a < c)
            System.out.println(a);
        else if (b < a && b < c)
            System.out.println(b);
        else
            System.out.println(c);
    }

    public static void main(String[] args) {
        int res1 = sum1(3, 3, 3);
        System.out.println(res1);

        sum2(5, 5);

        max(1569, 500000, 8);

        min(1025, 3, 50);
    }
}
