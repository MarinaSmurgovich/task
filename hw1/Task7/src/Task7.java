/**
 * Задание № 7
 * <p>
 * 1) Написать функцию определения количества дней месяца месяца по его номеру.
 * То есть, в функцию будет подано число. Если это число находится
 * в диапазоне 1 - 12, то нам нужно соответствующее количество дне для месяца и
 * его название.
 * Если же было введено число, которое не попадает в заданный диапазон,
 * то нужно вывести сообщение с ошибкой ("Число не в диапазоне").
 * <p>
 * Решение должно быть представлено в двух вариантах в отдельных функциях:
 * - с использованием if-else.
 * Подсказка: можно несколько раз использовать логическое "ИЛИ"(||)
 * - с использованием switch-case
 * <p>
 * 2) Произвести вызов функций в main
 * <p>
 * Пример вызова:
 * printMonthWithIfElse(12)
 * printMonthWithIfElse(44)
 */

public class Task7 {

    public static void printDaysInMonthWithIfElse(int monthNumber) {
        if (monthNumber == 1 || monthNumber == 3 || monthNumber == 5 ||
                monthNumber == 7 || monthNumber == 8 ||
                monthNumber == 10 || monthNumber == 12)
            System.out.println("31");
        else if (monthNumber == 4 || monthNumber == 6 || monthNumber == 9 ||
                monthNumber == 11)
            System.out.println("30");
        else if (monthNumber == 2)
            System.out.println("28");

        if (monthNumber == 1)
            System.out.println("Январь");
        else if (monthNumber == 2)
            System.out.println("Февраль");
        else if (monthNumber == 3)
            System.out.println("Март");
        else if (monthNumber == 4)
            System.out.println("Арель");
        else if (monthNumber == 5)
            System.out.println("Май");
        else if (monthNumber == 6)
            System.out.println("Июнь");
        else if (monthNumber == 7)
            System.out.println("Июль");
        else if (monthNumber == 8)
            System.out.println("Август");
        else if (monthNumber == 9)
            System.out.println("Сентябрь");
        else if (monthNumber == 10)
            System.out.println("Октябрь");
        else if (monthNumber == 11)
            System.out.println("Ноябрь");
        else if (monthNumber == 12)
            System.out.println("Декабрь");
        else
            System.out.println("Число не в диапазоне");
    }

    public static void printDaysInMonthWithSwitchCase(int monthNumber) {
        switch (monthNumber) {
            case 1:
                System.out.println("31");
                System.out.println("Январь");
                break;
            case 2:
                System.out.println("28");
                System.out.println("Февраль");
                break;
            case 3:
                System.out.println("31");
                System.out.println("Март");
                break;
            case 4:
                System.out.println("30");
                System.out.println("Апрель");
                break;
            case 5:
                System.out.println("31");
                System.out.println("Май");
                break;
            case 6:
                System.out.println("30");
                System.out.println("Июнь");
                break;
            case 7:
                System.out.println("31");
                System.out.println("Июль");
                break;
            case 8:
                System.out.println("31");
                System.out.println("Август");
                break;
            case 9:
                System.out.println("30");
                System.out.println("Сентябрь");
                break;
            case 10:
                System.out.println("31");
                System.out.println("Октябрь");
                break;
            case 11:
                System.out.println("30");
                System.out.println("Ноябрь");
                break;
            case 12:
                System.out.println("31");
                System.out.println("Декабрь");
                break;
            default:
                System.out.println("Число не в диапазоне");
        }
    }

    public static void main(String[] args) {

        printDaysInMonthWithIfElse(8);
        printDaysInMonthWithIfElse(44);

        System.out.println();

        printDaysInMonthWithSwitchCase(2);
        printDaysInMonthWithSwitchCase(44);

    }
}
