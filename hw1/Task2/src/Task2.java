/**
 * Задание № 2
 * <p>
 * ++++ 1) Написать алгоритм проверки числа на четность.
 * в методе main вывести сообщение:
 * - System.out.println("Четное"), если введенное число было четным,
 * то есть функция вернула true
 * - System.out.println("Нечетное"), если введенное число было нечетным,
 * то есть функция вернула false
 * <p>
 * +++2) Написать фукнцию расчета среднего числа между 4-мя значениями.
 * Результат вывести в консоль в main
 * <p>
 * ++++ 3) переделайте код метода clearOperator1() так,
 * чтобы использовались операции +=, -=, *=, /=.
 * Количество строк кода при этом не должно измениться.
 * <p>
 * +++ 4) Переделайте этот код метода clearOperator2(),
 * чтобы в нем использовались операции ++ и --.
 * Количество строк кода при этом не должно измениться.
 * <p>
 * ++++ 5) Вычислить выражения (записать в пригодной для java форме):
 * знак "/" - дробь. Вычисления можно сделать либо отдельной функцией,
 * либо в методе main.
 * <p>
 * a) (1/4 + 5/8 - 1) * 9 - 3
 * b) 9 + 3.6 + (33/(48*5/3))
 * c) 10 * 1/2 + (48*5/3)
 */

public class Task2 {

    public static boolean isEvenNumber(int number) {
        /**
         * NOTE! Лишние скобки в операции. Можно сделать так:
         * return number % 2 == 0;
         */
        return number % 2 == 0;
    }

    public static double getAvgNumber(int x, int y, int z, int l) {
        return (x + y + z + l) / 4.0;
    }

    public static void clearOperator1() {
        int num = 47;
        num += 7;
        num -= 18;
        num *= 10;
        num /= 15;
        System.out.println("clearOperator1 - " + num);
    }

    public static void clearOperator2() {
        int num = 47;
        num += 1;
        num -= 1;
        num += 1;
        num -= 1;
        System.out.println("clearOperator2 - " + num);
    }

    public static void main(String[] args) {
        // четность числа
        int a = 6;
        if (isEvenNumber(a))
            System.out.println("Четное");
        else
            System.out.println("Нечетное");

        // среднее значение суммы 4 чисел
        double b = getAvgNumber(4, 5, 6, 7);
        System.out.println("Средннее значание 4-х чисел - " + b);

        clearOperator1();
        clearOperator2();

        // a) (1/4 + 5/8 - 1) * 9 - 3
        double res1 = (1.0 / 4.0 + 5.0 / 8.0 - 1.0) * 9.0 - 3.0;
        System.out.println("res1: " + res1);

        // b) 9 + 3.6 + (33/(48*5/3))
        double res2 = 9.0 + 3.6 + (33.0 / (48.0 * 5.0 / 3.0));
        System.out.println("res2: " + res2);

        // c) 10 * 1/2 + (48*5/3)
        double res3 = 10.0 * 1.0 / 2.0 + (48.0 * 5.0 / 3.0);
        System.out.println("res3: " + res3);
    }
}
