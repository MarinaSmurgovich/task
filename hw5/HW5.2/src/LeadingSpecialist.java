public class LeadingSpecialist extends Specialist {
    private String responsibility;

    public LeadingSpecialist(String fullName, int age, String position, String duties, int salary, String responsibility) {
        super(fullName, age, position, duties, salary);
        this.responsibility = responsibility;
    }

    public String getResponsibility() {
        return responsibility;
    }

    public void setResponsibility(String responsibility) {
        this.responsibility = responsibility;
    }

    /** NOTE! Нет необходимости переопередять этот метод.
     * Так как ты все равно вызываешь только метод супер-класса.
     */
    public void introduceYourself() {
        super.introduceYourself();
    }

    @Override
    public void doJob(int day) {
        System.out.println("Ответственность за " + getResponsibility());
        super.doJob(day);
    }
}
