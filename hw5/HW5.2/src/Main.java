public class Main {
    public static void main(String[] args) {
        Person person = new Person("Иванов Иван Иванович", 30);
        person.introduceYourself();

        System.out.println();

        Trainee trainee = new Trainee("Петров Петя", 20, "Стажер");
        trainee.introduceYourself();
        trainee.doJob();

        System.out.println();

        Specialist specialist = new Specialist("Степанов Степан Степанович", 27, "Специалист", "Работа над проектом, простые задачи", 20);
        specialist.introduceYourself();
        specialist.doJob(10);

        System.out.println();

        LeadingSpecialist leadingSpecialist = new LeadingSpecialist("Семенов Иван Юрьевич", 30, "Ведущий специалист", "Работа над проектом", 40, "соблюдение сроков выполнения проекта");
        leadingSpecialist.introduceYourself();
        leadingSpecialist.doJob(20);

        System.out.println();

        ProjectManager projectManager = new ProjectManager("Васильев Константин", 30, "Руководитель проекта", "Координация проекта", 60, "выполнение проекта");
        projectManager.introduceYourself();
        projectManager.doJob(20, 6);

        System.out.println();

        Director director = new Director("Кузнецов Александр", 35, "Директор", "Руководить организацией, получать зарплату", 100, "работу о организации");
        director.introduceYourself();
        director.doJob(19);
    }
}
