public class PassengerCar extends Car{
    private int numberOfSeats;

    public PassengerCar(String title, int year, String engine, int engineVolume, int numberOfSeats) {
        super(title, year, engine, engineVolume);
        this.numberOfSeats = numberOfSeats;
    }
    public void introduceYourSelf () {
        System.out.println(getTitle() + getYear() + " года с двигателем " + getEngine() +" "+ getEngineVolume() + " куб.см, число мест " + numberOfSeats);
    }
}
