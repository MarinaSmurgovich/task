package triangle;

import rectangle.Square;

public class Triangle extends Square {

    public Triangle(String name, double width) {
        super(name, width);
    }

    /** NOTE! Нет необходимости переопередять этот метод.
     * Так как ты все равно вызываешь только метод супер-класса.
     */
    public void sayYourName() {
        super.sayYourName();
    }

    @Override
    public double area() {
        double area =(Math.sqrt(3) / 4) *  (width * width);
        System.out.println(area);
        return (Math.sqrt(3) / 4) *  (width * width);
    }

    @Override
    public void perimeter() {
        System.out.println("Периметр равен: " + width * 3);
    }
}
