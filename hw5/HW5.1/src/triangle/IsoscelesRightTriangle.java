package triangle;

public class IsoscelesRightTriangle extends Triangle {
    protected double hypotenuse;

    public IsoscelesRightTriangle(String name, double width, double hypotenuse) {
        super(name, width);
        this.hypotenuse = hypotenuse;
    }

    /** NOTE! Нет необходимости переопередять этот метод.
     * Так как ты все равно вызываешь только метод супер-класса.
     */
    public void sayYourName() {
        super.sayYourName();
    }

    @Override
    public double area() {
        double area = 0.5 * width * width;
        System.out.println(area);
        return 0.5 * width * width;
    }

    @Override
    public void perimeter() {
        System.out.println("Периметр равен: " + (width * 2 + hypotenuse));
    }
}
