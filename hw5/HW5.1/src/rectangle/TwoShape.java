package rectangle;

public class TwoShape {
    private String name;

    public TwoShape(String name) {
        this.name = name;
    }

    public void sayYourName() {
        System.out.println("Название фигуры - " + name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
