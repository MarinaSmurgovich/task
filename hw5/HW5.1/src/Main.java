import rectangle.Rectangle;
import rectangle.Square;
import triangle.IsoscelesRightTriangle;
import triangle.RightTriangle;
import triangle.Triangle;

public class Main {
    public static void main (String[] args) {
            Square square = new Square("Квадрат", 5.0);
            square.sayYourName();
            square.area();
            square.perimeter();

            Rectangle rectangle = new Rectangle("Прямоугольник", 5.0, 6.0);
            rectangle.sayYourName();
            rectangle.area();
            rectangle.perimeter();

            Triangle triangle = new Triangle("Треугольник", 5.0);
            triangle.sayYourName();
            triangle.area();
            triangle.perimeter();

            IsoscelesRightTriangle isoscelesRightTriangle = new IsoscelesRightTriangle("Равнобедренный прямоугодьный треугольник", 5.0, 6);
            isoscelesRightTriangle.sayYourName();
            isoscelesRightTriangle.area();
            isoscelesRightTriangle.perimeter();

            RightTriangle rightTriangle = new RightTriangle("Прямоугольный теругольник", 6, 9, 8);
            rightTriangle.sayYourName();
            rightTriangle.area();
            rightTriangle.perimeter();
    }
}
