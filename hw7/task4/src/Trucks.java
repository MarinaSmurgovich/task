public class Trucks extends Car {
    private double carryingCapacity;

    public Trucks(String title, int year, String engine, int engineVolume, double carryingCapacity) {
        super(title, year, engine, engineVolume);
        this.carryingCapacity = carryingCapacity;
    }

    public void introduceYourSelf () {
        System.out.println(getTitle() + getYear() + " года с двигателем " + getEngine() +" "+ getEngineVolume() + " куб.см, грузоподьемность " + carryingCapacity + " тонн");
    }
}


