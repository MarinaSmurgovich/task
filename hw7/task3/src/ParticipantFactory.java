import java.util.*;

public class ParticipantFactory {
    private int number;

    public static int numberOfParticipants() {
        System.out.println("Введите чило игроков: ");
        Scanner input = new Scanner(System.in);
        int number = input.nextInt();
        return number;
    }

    public static Map<Integer, ArrayList> madeParticipants(int number) {
        Map<Integer, ArrayList> participants = new TreeMap<>();
        for (int i = 1; i <= number; i++) {
            participants.put(i, NumberGenerator.random(3));
        }
        Set<Map.Entry<Integer, ArrayList>> set = participants.entrySet();
        for (Map.Entry<Integer, ArrayList> el : set) {
            System.out.println("Игрок " + el.getKey() + " - Числа: " + el.getValue());
        }
        return participants;
    }

}
