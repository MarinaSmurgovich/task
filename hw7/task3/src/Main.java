import java.util.*;

public class Main {
    public static void main(String[] args) {
        int res;
        do {
            int number = ParticipantFactory.numberOfParticipants();
            Map<Integer, ArrayList> participants = new TreeMap<>(ParticipantFactory.madeParticipants(number));
            int n = Participant.numberOfParticipant();
            System.out.print("Выигрышные номера: ");
            ArrayList<Integer> winnerNumbers = new ArrayList<>(NumberGenerator.random(10));
            System.out.println(winnerNumbers);

            System.out.print("Участники, которые выиграли: ");
            Set<Map.Entry<Integer, ArrayList>> set = participants.entrySet();
            ArrayList<Integer> winners = new ArrayList<>();
            for (Map.Entry<Integer, ArrayList> el : set) {
                int count = 0;
                for (Object i : el.getValue()) {
                    for (Object v : winnerNumbers) {
                        if (v == i) {
                            count += 1;
                        }
                    }
                }
                if (count == 3) {
                    winners.add(el.getKey());
                    System.out.print((el.getKey()) + ", ");
                }
            }
            System.out.println();
            for (int t : winners) {
                if (t == n)
                    System.out.println("Вы победили!!!");
            }
            res = Participant.menu();
        }
        while (res != 2);
    }
}
